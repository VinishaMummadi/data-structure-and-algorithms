/**
 * 
 *CSCI803 ASSIGNMENT 2 – Simulating queuing and service of airline passengers
 * Name: Vishnu Vinisha Mummadi
 * Student Login: vvm863
 *
 */

#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstdlib>
#include <sstream>

using namespace std;

/* MAX QUEUE SIZE */
const int QUE_SIZE = 500;

/* ENUM DEFINITIONS */
enum EventType {
    ARRIVALS = 0,
    FINISHED = 1
};

enum TravelClass {
    TOURISTS = 0,
    BUSINESS = 1
};

/* STRUCT DEFINITIONS */
struct Passengers {
    double timeofArrival;
    int classIndicator;
    double serviceTime;

    Passengers()
    {
        timeofArrival = 0;
        classIndicator = TOURISTS;
        serviceTime = 0;
    }

    ~Passengers()
    {
    }
};

struct Server {
    int classIndicator;
    bool busyFlag;
    double nextSvcTime;
    double svrIdleTime;
    double lastServicedTime;

    Server()
    {
        classIndicator = TOURISTS;
        busyFlag = false;
        svrIdleTime = 0;
        nextSvcTime = 0;
        lastServicedTime = 0;
    }

    ~Server()
    {
    }
};

struct EventQueue {
    Passengers** data;
    int begin;
    int end;
    int classIndicator;
    int size;
    int maxSize;
    double overallQueueSize;
    double overallQueueTime;
    int paxInQueue;

    EventQueue()
    {
        data = new Passengers*[QUE_SIZE];
        begin = end = size = maxSize = paxInQueue = 0;
        classIndicator = TOURISTS;
        overallQueueTime = overallQueueSize = 0;
    }

    void enqueue(Passengers* passengers)
    {
        data[(begin + size) % QUE_SIZE] = passengers;
        size++;
        paxInQueue++;
        overallQueueSize += size;
        if (size > maxSize) {
            maxSize = size;
        }
    }

    Passengers* dequeue()
    {
        Passengers* passengers = data[begin];
        begin = (begin + 1) % QUE_SIZE;
        size--;
        return passengers;
    }

    bool isEmpty()
    {
        return size == 0;
    }

    ~EventQueue()
    {
        while (size > 0) {
            delete dequeue();
        }
        delete[] data;
    }
};

struct Event {
    Passengers* pax;
    Server* server;
    double nextSvcTime;
    int typ;

    Event()
    {
        pax = NULL;
        server = NULL;
        nextSvcTime = 0;
        typ = ARRIVALS;
    }

    ~Event()
    {
    }
};

struct EventHeap {
    Event** data;
    int sizeOfHeap;
    int sizeOfArray;

    EventHeap()
    {
        data = new Event*[sizeOfArray];
        sizeOfHeap = 0;
        sizeOfArray = 1000;
    }

    Event* getNxtEvent()
    {
        if (sizeOfHeap == 0)
            return NULL;
        else
            return data[0];
    }

    int getrootIndex(int index)
    {
        return (index - 1) / 2;
    }

    int getRightNodeIndex(int index)
    {
        return 2 * index + 2;
    }

    int getLeftNodeIndex(int index)
    {
        return 2 * index + 1;
    }

    void addToHeap(Event* event)
    {
        if (sizeOfHeap == sizeOfArray)
            cout << "Heap is not large enough"
                 << "\n";
        else {
            sizeOfHeap++;
            data[sizeOfHeap - 1] = event;
            siftUp(sizeOfHeap - 1);
        }
    }

    void siftUp(int index)
    {
        int rootIndex;
        Event* temp;
        if (index != 0) {
            rootIndex = getrootIndex(index);
            if (data[rootIndex]->nextSvcTime > data[index]->nextSvcTime) {
                temp = data[rootIndex];
                data[rootIndex] = data[index];
                data[index] = temp;
                siftUp(rootIndex);
            }
        }
    }

    void siftDown(int index)
    {
        Event* temp;
        int leftNodePos, rightNodePos, minInd;
        leftNodePos = getLeftNodeIndex(index);
        rightNodePos = getRightNodeIndex(index);

        if (rightNodePos >= sizeOfHeap) {
            if (leftNodePos >= sizeOfHeap)
                return;
            else
                minInd = leftNodePos;
        }
        else {
            if (data[leftNodePos]->nextSvcTime <= data[rightNodePos]->nextSvcTime)
                minInd = leftNodePos;
            else
                minInd = rightNodePos;
        }
        if (data[index]->nextSvcTime > data[minInd]->nextSvcTime) {
            temp = data[minInd];
            data[minInd] = data[index];
            data[index] = temp;
            siftDown(minInd);
        }
    }

    void removeMin()
    {
        Event* temp;
        if (sizeOfHeap == 0)
            cout << "HEAP empty!!!";
        else {
            temp = data[0];
            data[0] = data[sizeOfHeap - 1];
            data[sizeOfHeap - 1] = temp;
            sizeOfHeap--;
            if (sizeOfHeap > 0) {
                siftDown(0);
            }
            delete temp;
        }
    }

    ~EventHeap()
    {
        while (sizeOfHeap > 0) {
            removeMin();
        }
        delete[] data;
    }
};

/* Metrics variables */
double curServerTime, initialServiceStartTime, finalServiceCmpletionTime, ttlServiceTimeTaken;
int noOfBusinessServerCount, noOfTouristServerCount,totalPassengersServed, avgQueueLength;

/* Input data file*/
ifstream inputDataFile;

Server*** servers;
EventQueue** eventQueue;
EventHeap heap;

/* Reads the first line in the data file to obtain no: of servers */
void readNoOfAvailableServers(const char* dataFileName)
{

    string row, block;
    finalServiceCmpletionTime = avgQueueLength = 0;
    totalPassengersServed = 0;

    inputDataFile.open(dataFileName);

    if (inputDataFile.is_open()) {
        if (!inputDataFile.eof()) {
            getline(inputDataFile, row);
            stringstream lineStr(row);

            getline(lineStr, block, ' '); // Reading no: of business server count.
            noOfBusinessServerCount = atoi(block.c_str());

            getline(lineStr, block, ' '); // Reading no: of tourist server count.
            noOfTouristServerCount = atoi(block.c_str());
        }
        else {
            cout << "WARN: FILE " << dataFileName << " IS EMPTY"
                 << "\nEXITING...\n";
            exit(0);
        }
    }
    else {
        cout << "ERORR: UNABLE TO OPEN FILE " << dataFileName << "\nEXITING...\n";
        exit(0);
    }
}

/* Funtion to return the free server from the available servers */
Server* getNextFreeClassServer(int classIndicator, bool businessServerTakeTouristRequest)
{
    int numOfServers;

    if (classIndicator == BUSINESS) 
        numOfServers = noOfBusinessServerCount;
    else 
        numOfServers = noOfTouristServerCount;

    for (int i = 0; i < numOfServers; i++) {
        if (!servers[classIndicator][i]->busyFlag) 
            return servers[classIndicator][i];
    }
    if (classIndicator == TOURISTS && businessServerTakeTouristRequest) {
        for (int j = 0; j < noOfBusinessServerCount; j++) {
            if (!servers[BUSINESS][j]->busyFlag) 
                return servers[BUSINESS][j];
        }
    }

    return NULL;
}

/* Reads and return the passengers data from input file */
Passengers* getNextPax()
{

    Passengers* passengers;
    string row, block;

    if (inputDataFile.is_open()) {

        if (!inputDataFile.eof()) {
            getline(inputDataFile, row);
            passengers = new Passengers();
            stringstream lineStr(row);

            // Reading Arrival Time
            getline(lineStr, block, ' ');
            passengers->timeofArrival = strtod(block.c_str(), NULL);

            // Reading Class Indicator
            getline(lineStr, block, ' ');
            passengers->classIndicator = atoi(block.c_str());

            // Reading Service Time
            getline(lineStr, block, ' ');
            passengers->serviceTime = strtod(block.c_str(), NULL);

            // Checking if end of data record is reached i.e: 0 0 0
            if (passengers->timeofArrival == 0 && passengers->classIndicator == 0 && passengers->serviceTime == 0) {
                inputDataFile.close();
                delete passengers;
            }
            else {
                return passengers;
            }
        }
    }
    return NULL;
}

/* Function to intialzie the Server & EventQueue struct variables based on no: of airlines server */
void intializeAirlineClassServers()
{
    servers = new Server**[2];
    servers[0] = new Server*[noOfTouristServerCount];
    servers[1] = new Server*[noOfBusinessServerCount];

    for (int x = 0; x < noOfTouristServerCount; x++) {
        servers[0][x] = new Server();
    }
    for (int y = 0; y < noOfBusinessServerCount; y++) {
        servers[1][y] = new Server();
        servers[1][y]->classIndicator = BUSINESS;
    }
    eventQueue = new EventQueue*[2];
    eventQueue[0] = new EventQueue();
    eventQueue[1] = new EventQueue();
    eventQueue[1]->classIndicator = BUSINESS;
}

/* Helper function to print on the console */
void printMetricOnConsole(const char* msg, double value, int width)
{
    cout << msg << std::fixed << setprecision(2) << setw(width) << value << "\n";
}

void displayMetrics(int pass, bool businessServerTakeTouristRequest)
{
    double avgTtlTimeInQueOverall, avgTtlTimeInTouristQue, avgTtlTimeInFirstBusinessQue, avgLengOfQueueOverall, avgLengOfTouristQueue, avgLengOfFirstBusinessQueue;

    // Calculating the average metrics
    if (eventQueue[TOURISTS]->paxInQueue + eventQueue[BUSINESS]->paxInQueue == 0) {
        avgTtlTimeInQueOverall = avgLengOfQueueOverall = 0;
    }
    else {
        avgTtlTimeInQueOverall = (eventQueue[TOURISTS]->overallQueueTime + eventQueue[BUSINESS]->overallQueueTime) / (eventQueue[TOURISTS]->paxInQueue + eventQueue[BUSINESS]->paxInQueue);
        avgLengOfQueueOverall = (eventQueue[TOURISTS]->overallQueueSize + eventQueue[BUSINESS]->overallQueueSize) / (eventQueue[TOURISTS]->paxInQueue + eventQueue[BUSINESS]->paxInQueue);
    }
    if (eventQueue[TOURISTS]->paxInQueue == 0) {
        avgTtlTimeInTouristQue = avgLengOfTouristQueue = 0;
    }
    else {
        avgTtlTimeInTouristQue = eventQueue[TOURISTS]->overallQueueTime / eventQueue[TOURISTS]->paxInQueue;
        avgLengOfTouristQueue = eventQueue[TOURISTS]->overallQueueSize / eventQueue[TOURISTS]->paxInQueue;
    }
    if (eventQueue[BUSINESS]->paxInQueue == 0) {
        avgTtlTimeInFirstBusinessQue = avgLengOfFirstBusinessQueue = 0;
    }
    else {
        avgTtlTimeInFirstBusinessQue = eventQueue[BUSINESS]->overallQueueTime / eventQueue[BUSINESS]->paxInQueue;
        avgLengOfFirstBusinessQueue = eventQueue[BUSINESS]->overallQueueSize / eventQueue[BUSINESS]->paxInQueue;
    }

    int overallMax = eventQueue[BUSINESS]->maxSize;
    if (eventQueue[TOURISTS]->maxSize > eventQueue[BUSINESS]->maxSize) {
        overallMax = eventQueue[TOURISTS]->maxSize;
    }

    const char* title;
    if (!businessServerTakeTouristRequest) {
        title = "Pass 1: Business servers exclusively serve business class\n";
    }
    else {
        title = "Pass 2: Idle business servers may serve tourist class\n";
    }

    cout << "\n\n" << title << "\n";

    cout << "Number of people served:  "
         << "\t\t\t" << totalPassengersServed << "\n";
    printMetricOnConsole("Time last service request completed: ", finalServiceCmpletionTime, 17);

    cout << "\nBusiness class customers: \n";
    printMetricOnConsole("Average total service time: ", (double)(ttlServiceTimeTaken / totalPassengersServed), 25);
    printMetricOnConsole("Average total time in queue:", avgTtlTimeInFirstBusinessQue, 24);
    printMetricOnConsole("Average size of queue:", avgLengOfFirstBusinessQueue, 30);
    cout << "Maximum number queued:" << setw(27) << eventQueue[BUSINESS]->maxSize << "\n";

    cout << "\nTourist class customers: \n";
    printMetricOnConsole("Average total service time: ", (double)(ttlServiceTimeTaken / totalPassengersServed), 25);
    printMetricOnConsole("Average total time in queue:", avgTtlTimeInTouristQue, 24);
    printMetricOnConsole("Average size of queue:", avgLengOfTouristQueue, 30);
    cout << "Maximum number queued:" << setw(27) << eventQueue[TOURISTS]->maxSize << "\n";

    cout << "\nAll customers: \n";
    printMetricOnConsole("Average total service time: ", (double)(ttlServiceTimeTaken / totalPassengersServed), 25);
    printMetricOnConsole("Average total time in queue:", avgTtlTimeInQueOverall, 24);
    printMetricOnConsole("Average size of queue:", avgLengOfQueueOverall, 30);
    cout << "Maximum number queued:" << setw(27) << overallMax << "\n";

    cout << "\nBusiness class servers: \n";
    for (int x = 0; x < noOfBusinessServerCount; x++) {
        string msg = "Total idle time of business server " + to_string(x + 1) + ": ";
        double value = servers[BUSINESS][x]->svrIdleTime;
        printMetricOnConsole(msg.c_str(), value, 16);
    }

    cout << "\nTourist class servers: \n";
    for (int y = 0; y < noOfTouristServerCount; y++) {
        string msg = "Total idle time of tourist class server " + to_string(y + 1) + ": ";
        double value = servers[TOURISTS][y]->svrIdleTime;
        printMetricOnConsole(msg.c_str(), value, 11);
    }
}

void runSimulation(const char* dataFileName, int pass, bool businessServerTakeTouristRequest)
{
    int numOfServers, classIndicator;
    Server* nextAvailServer;
    Event* nextEvent;
    Passengers* nextPassenger;

    readNoOfAvailableServers(dataFileName);
    nextPassenger = getNextPax();

    //Safety check for data file should have atleast 1 passenger.
    if (nextPassenger == NULL) {
        cout << "Data file " << dataFileName << " should have at least 1 airline passenger to proceed.\n";
        exit(0);
    }

    curServerTime = initialServiceStartTime = nextPassenger->timeofArrival;
    ttlServiceTimeTaken = 0;

    intializeAirlineClassServers();

    nextEvent = new Event();
    nextEvent->pax = nextPassenger;
    nextEvent->nextSvcTime = nextPassenger->timeofArrival;
    heap.addToHeap(nextEvent);

    do {
        if (nextEvent->typ == ARRIVALS) { // For Arrival find the next available server.
            curServerTime = nextEvent->nextSvcTime;
            // Getting the next available free server (Taking into consideration first or second pass using 'businessServerTakeTouristRequest')
            nextAvailServer = getNextFreeClassServer(nextEvent->pax->classIndicator, businessServerTakeTouristRequest);

            /*
                If a server is available
				Change the server state to busy and calculate service time plus idle time.
				Process the arrived event (i.e Passenger) as finished.
                Else no server available then add to the event queue.
            */
            if (nextAvailServer != NULL) {

                nextAvailServer->busyFlag = true; // Changing server to busy state.
                // Calculating  service time and idle time.
                nextAvailServer->nextSvcTime = (curServerTime + nextEvent->pax->serviceTime);
                nextAvailServer->svrIdleTime += (curServerTime - nextAvailServer->lastServicedTime);

                nextEvent->typ = FINISHED;
                nextEvent->server = nextAvailServer;
                nextEvent->nextSvcTime = (curServerTime + nextEvent->pax->serviceTime);

                heap.siftDown(0);
            }
            else { // If no available server found then adding it to EventQueue
                eventQueue[nextEvent->pax->classIndicator]->enqueue(nextEvent->pax);
                heap.removeMin();
            }
        }
        else { // When Completed event type
            /*
                When 'passenger finish' is being processed in server
                setting server busy status to false and calculating service metrics.
            */
            totalPassengersServed++; // Increment number of passengers served.

            curServerTime = nextEvent->nextSvcTime;
            finalServiceCmpletionTime = curServerTime;
            ttlServiceTimeTaken += nextEvent->pax->serviceTime;

            nextAvailServer = nextEvent->server;
            classIndicator = nextEvent->server->classIndicator;
            heap.removeMin();

            nextAvailServer->lastServicedTime = curServerTime;
            nextAvailServer->busyFlag = false;

            if (classIndicator == BUSINESS) {
                numOfServers = noOfBusinessServerCount;
            }
            else {
                numOfServers = noOfTouristServerCount;
            }

            for (int i = 0; i < numOfServers && !nextAvailServer->busyFlag; i++) {
                if (!eventQueue[classIndicator]->isEmpty()) {
                    nextAvailServer->svrIdleTime += (curServerTime - nextAvailServer->lastServicedTime);
                    nextAvailServer->lastServicedTime = nextAvailServer->nextSvcTime;
                    nextAvailServer->busyFlag = true;

                    nextEvent = new Event();
                    nextEvent->pax = eventQueue[classIndicator]->dequeue();
                    nextEvent->server = nextAvailServer;
                    nextEvent->typ = FINISHED;
                    nextEvent->nextSvcTime = nextAvailServer->nextSvcTime = (curServerTime + nextEvent->pax->serviceTime);
                    heap.addToHeap(nextEvent);
                    eventQueue[classIndicator]->overallQueueTime += (curServerTime - nextEvent->pax->timeofArrival);
                }
            }

            /*
                When the business class server are not busy and have all the event then
                set server status to busy to false and start process Toursit events from
                the Event Queue (2nd pass) and calculate server metrics.
            */
            if (!nextAvailServer->busyFlag && nextAvailServer->classIndicator == BUSINESS && eventQueue[BUSINESS]->size == 0 && businessServerTakeTouristRequest) {
                for (int i = 0; i < numOfServers && !nextAvailServer->busyFlag; i++) {
                    if (!eventQueue[classIndicator]->isEmpty()) {
                        nextAvailServer->svrIdleTime += (curServerTime - nextAvailServer->lastServicedTime);
                        nextAvailServer->lastServicedTime = nextAvailServer->nextSvcTime;
                        nextAvailServer->busyFlag = true;

                        nextEvent = new Event();
                        nextEvent->pax = eventQueue[TOURISTS]->dequeue();
                        nextEvent->server = nextAvailServer;
                        nextEvent->typ = FINISHED;
                        nextEvent->nextSvcTime = nextAvailServer->nextSvcTime = (curServerTime + nextEvent->pax->serviceTime);
                        heap.addToHeap(nextEvent);
                    }
                }
            }
        }
        /* Read the next passenger data from the file */
        nextPassenger = getNextPax();
        if (nextPassenger != NULL) {
            nextEvent = new Event();
            nextEvent->pax = nextPassenger;
            nextEvent->nextSvcTime = nextPassenger->timeofArrival;
            heap.addToHeap(nextEvent);
        }
        nextEvent = heap.getNxtEvent();
    } while (heap.sizeOfHeap > 0);

    inputDataFile.close();

    // Calculating idle time for Business Server
    for (int j = 0; j < noOfBusinessServerCount; j++) {
        servers[BUSINESS][j]->svrIdleTime += curServerTime - servers[BUSINESS][j]->lastServicedTime;
    }

    // Calculating idle time for Tourist Server
    for (int i = 0; i < noOfTouristServerCount; i++) {
        servers[TOURISTS][i]->svrIdleTime += curServerTime - servers[TOURISTS][i]->lastServicedTime;
    }

    displayMetrics(pass, businessServerTakeTouristRequest);

    // Cleaning up memeory
    for (int j = 0; j < noOfBusinessServerCount; ++j) {
        delete servers[BUSINESS][j];
    }
    delete[] servers[BUSINESS];

    for (int i = 0; i < noOfTouristServerCount; ++i) {
        delete servers[TOURISTS][i];
    }
    delete[] servers[TOURISTS];

    delete servers;
    delete eventQueue[0];
    delete eventQueue[1];
    delete eventQueue;
}

int main(int argc, char* argv[])
{

    // Using the file name if it is passed via commnad line
    if (argc == 2) { // arg passed
        runSimulation(argv[1], 1, false);
        runSimulation(argv[1], 2, true);
    }
    else { // Asking user for file name to read the data
        char dataFileName[50];
        cout << "Enter file name-";
        cin.getline(dataFileName, 50);

        runSimulation(dataFileName, 1, false);
        runSimulation(dataFileName, 2, true);
    }

    return 0;
}
