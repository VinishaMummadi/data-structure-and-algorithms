/**
 * 
 * CSCI203/CSCI803 ASSIGNMENT 3 – Single source-single destination shortest path 
 * 
 * Name: Vishnu Vinisha Mummadi
 * Student Login: vvm863
 *
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
using namespace std;


//'Result' structure used to store results from each algorthim pass and also used to print the statistics.

struct Result {                               

	int* path;
	int* parent;
	double* dist;
	double path_weight;
	bool* candidate_set;
	int ver;
	int path_size;

	Result();
	Result(int ver, int src);
	int getPathLength();
	double getPathWeight();
	void addVerticeToPath(int src, int dest, int ind);
	void printMetrics();
	void deleteResult();
};

#include <climits>
Result::Result() {}
//Constructor to initialize
Result::Result(int ver, int src) {
	path_size = 0;
	path_weight = 0;
	dist = new double[ver];
	parent = new int[ver];
	candidate_set = new bool[ver];
	path = new int[ver];
	this->ver = ver;
	for (int i = 0;i < ver;i++)
	{
		dist[i] = INT_MAX;
		candidate_set[i] = true;
		path[i] = 0;
		parent[i] = src;
		
	}
}

//Creating path from given result data.
void Result::addVerticeToPath(int src, int dest, int ind) {

	//Traversing path from destination node to starting node
	path[path_size++] = dest;
	int temp = parent[dest];

	//Stop processing at start node
	while (temp != src) {
		path[path_size++] = temp;
		temp = parent[temp];
	}
	path[path_size++] = temp;

	//Reversal of path to finally get source to destination path
	int a, b;
	for (a = 0, b = path_size- 1; a < b; a++, b--)
	{
		int tmp;
		tmp = path[b];
		path[b] = path[a];
		path[a] = tmp;
	}
}

//Printing required metrics (Path traversed and Length of path)
void Result::printMetrics() {

	cout << "Path Traversed: ";
	for (int i = 0; i < path_size; i++) {
		cout << path[i] ;
		if(i!=path_size-1)
			cout << " --> ";
	}
    cout << "\nLength of the path: " << path_weight <<"\n";
}

int Result::getPathLength() {
	return path_size;
}

double Result::getPathWeight() {
	return path_weight;
}

//Destructor to clean up memory after processing complete
void Result::deleteResult() {
	delete[] dist;
	delete[] parent;
	delete[] path;
	delete[] candidate_set;
}

//Graph structure to store the graph data (vertices,edges, coordinates and weights) from file and run Dijkstra's algorithm.
struct Graph {

	double *x;
	double* y;
	int ver;
	char* vertex;
	double **mat;
	int dest;
	Result* results;
	int resultSize;
	
	Graph(int ver);
	~Graph();
	void addEdge(int src, int dest, double path_weight);
	void addCoordinates(int node, double xpos, double ypos, int ind);
	int minimumDistance(int resultInd);
	void dijkstra(int src, int dest);
	void secondShortestPath(int src, int destination);

};

//Initializing the variables with details read from data file
Graph::Graph(int ver) {
	ver = ver + 1 ; // Taking index starting from 1
	this->ver = ver;
     
	//Result storage array for each algo run
	results = new Result [ver];
	resultSize =  0;
	mat = new double* [ver];

	//x & y coordinates array with vertex value
	x = new double[ver];
	y = new double[ver];
	vertex = new char[ver];

	for (int i = 0; i < ver; i++)
	{
		mat[i] = new double[ver];
	}
}

Graph::~Graph() {
	//Clearing memory by delete all the arrays.

	for (int z = 0; z < ver; z++) {
		delete[] mat[z];
	}

	delete[]  mat;
	delete[]  results;
	delete[]  x;
	delete[]  y;
	delete[]  vertex;
}

//Function to locate next node at minimumn distance.
int Graph::minimumDistance(int resultIndex)
{ 
	int minDist = INT_MAX;
	int minInd = 0;

	for (int end = 0; end < ver; end++)
	{
		if (results[resultIndex].candidate_set[end] == true && results[resultIndex].dist[end] <= minDist) {
			minDist = results[resultIndex].dist[end];
			minInd = end;
		}
	}
	return minInd;
}

//Function to create adjacency matrix from the edge & weight data read from input file
void Graph::addEdge(int src, int dest, double path_weight) {

		mat[src][dest] = path_weight;
}

//Function to add x and y co-ordinates for each node.
void Graph::addCoordinates(int node, double xpos, double ypos, int index) {
	vertex[node] = node;
	x[index] = xpos;
	y[index] = ypos;
}
//Function to implement the Dijkstra's algo from src node to dest node
void Graph::dijkstra(int src, int dest)
{
	Result res(ver, src);
	results[resultSize] = res;

	//Initial keeping dist value as 0
	results[resultSize].dist[src] = 0;
	results[resultSize].candidate_set[src] = false;
	int candidateLength = ver-1;


	for (int a = 0; a < ver; a++) {

		if (mat[src][a] != 0) {
			results[resultSize].dist[a] = mat[src][a];
		}
	}

    int loopCount = 1;
	bool destinationNode = false;

	//Traversing the graph upto the stage till destination node has been reached or else all nodes present dataFile graph have been visited once. 
	while (candidateLength > 0 && !destinationNode) {
				
		//Finding the closet unvisited node to the source
		int start = minimumDistance(resultSize);
		
        if (start == dest)
			destinationNode = true;

	
		//Once the node is visited, removing it from candidate set.
		results[resultSize].candidate_set[start] = false;
		candidateLength--;


		//Updating the result distance array for all nodes that can be accessed from current ndoe.
		for (int end = 0; end <= ver; end++) {

			//The minimum distance from current node is updated when its less than distance present result array.
			if (results[resultSize].candidate_set[end] == true && results[resultSize].dist[start] + mat[start][end] < results[resultSize].dist[end] && mat[start][end]!=0 && results[resultSize].dist[start]!=INT_MAX) {
				
                results[resultSize].dist[end] = results[resultSize].dist[start] + mat[start][end];
				results[resultSize].parent[end] = start;
			}
		}
		
        loopCount++;		
	}

	results[resultSize].path_weight = results[resultSize].dist[dest];

	//Storing the result path based on the start array to reach the destination node.
	results[resultSize].addVerticeToPath(src, dest, resultSize);
	resultSize++;
}


//Function to find the second shortest path
void Graph::secondShortestPath(int src, int destination) {
	/*
	To find the second shortest path for each edge on shortest path first it is removed and then shortest distance is calucalted.
	At the end of the iteration the shortest path after removing one edge is the second shortest path.
	*/

	int start, temp, minLength, minWgt=INT_MAX, minInd, totalVisitedEdges=0, counter=0;
	
	int dest = destination;
	int beginLoop = resultSize;
	int numOfLoops = results[0].path_size + beginLoop-1;

	for (int w = resultSize; w < numOfLoops; w++) {
		start = results[0].path[counter];
		dest = results[0].path[counter+1];
		counter++;

		//Temp saving edge to be replaced after running algorithm.
		temp = mat[start][dest];
		mat[start][dest] = 0;

		//Calling the algo funtion after removing one of the edge.
		dijkstra(src, destination);
		
		if (results[w].getPathWeight()<=minWgt) {
			minLength = results[w].getPathLength();
			minWgt = results[w].getPathWeight();
			minInd = w;
		}

		//Replacing edge with previously removed edge
		mat[start][dest] = temp;
	}
		
	results[minInd].printMetrics();
}

int main() {

	ifstream dataFile;
	char filename[50];

	cout << "Enter file name with graph data-";
	cin.getline(filename, 50);

	dataFile.open(filename);
	if (!dataFile.good()) {
		cout << "ERORR: UNABLE TO OPEN SPECIFIED FILE " << filename << "\nEXTITING\n";
		return -1;
	}
		
	string firstLine, block;
	int  nVertices, nEdges;

	getline(dataFile, firstLine);
	stringstream lineStr(firstLine);

	// Reading no: of vertices 
	getline(lineStr, block, '\t');
	nVertices= atoi(block.c_str());
		
	// Reading no: of edges
	getline(lineStr, block, '\t');
	nEdges = atoi(block.c_str());

	int vcount = 0, node;
	double x, y;

	Graph graphObject(nVertices);

	//Reading dataFile the Vertices triples data (Label, x & y coordinates)
	while (vcount < nVertices) {

		string line;
		getline(dataFile, line);
		stringstream verticesData(line);

		// Reading vertex label
		getline(verticesData, block, '\t');
		node = atoi(block.c_str());

		// Reading x coordinate
		getline(verticesData, block, '\t');
		x = atof(block.c_str());

		// Reading y coordinate
		getline(verticesData, block, '\t');
		y = atof(block.c_str());

		graphObject.addCoordinates(node, x, y, vcount);
		vcount++;

	}

	int startNode, endNode;
	double path_weight;

	int ecount=0;
		
	//Reading the Edges tripled data (Start & End label, Weight) and adding weights dataFile mat
	while(ecount < nEdges){
		
		string eline;
		getline(dataFile, eline);
		
		stringstream edgeData(eline);

		// Reading start vertex
		getline(edgeData, block, '\t');
		startNode = atoi(block.c_str());


		// Reading end vertext
		getline(edgeData, block, '\t');
		endNode = atoi(block.c_str());

		// Reading path weight
		getline(edgeData, block, '\t');
		path_weight = atof(block.c_str());
		

		graphObject.addEdge(startNode, endNode, path_weight);
		ecount++;
	} 

	//Reading the start and end goal vertices from last line dataFile text file
	int src, dest;
	string lastLine;
	getline(dataFile, lastLine);

	stringstream findData(lastLine);

	// Reading goal start vertex
	getline(findData, block, '\t');
	src = atoi(block.c_str());

	// Reading goal end vertex
	getline(findData, block, '\t');
	dest = atoi(block.c_str());
	dataFile.close();

	//Calling dijkstra's algorithm method to find the shortest path.
	graphObject.dijkstra(src,dest);

	cout << "\nShortest path with Dijkstra's' Algorithm";
	cout<< "\n-------------------------------------------";
	cout <<"\nShortest Path from " << src << " to "<< dest << "\n";
	graphObject.results[0].printMetrics();

	cout <<"\nSecond Shortest path from " << src << " to "<< dest;
	cout<< "\n---------------------------------------\n";
	graphObject.secondShortestPath(src,dest);

	//deleting the objects to prevent the memory leaks
	for (int i = 0; i < graphObject.resultSize; i++) {
		graphObject.results[i].deleteResult();
		}
}

